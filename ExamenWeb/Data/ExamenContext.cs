﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ExamenWeb.Models;

namespace ExamenWeb.Data
{
    public class ExamenContext : DbContext
    {
        public ExamenContext (DbContextOptions<ExamenContext> options)
            : base(options)
        {
        }

        public DbSet<ExamenWeb.Models.Celulares>? Celular { get; set; }
    }
}
