﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ExamenWeb.Data;
using ExamenWeb.Models;

namespace ExamenWeb.Pages.Celular
{
    public class IndexModel : PageModel
    {
        private readonly ExamenWeb.Data.ExamenContext _context;

        public IndexModel(ExamenWeb.Data.ExamenContext context)
        {
            _context = context;
        }

        public IList<Celulares> Celular { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Celular != null)
            {
                Celular = await _context.Celular.ToListAsync();
            }
        }
    }
}
