﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ExamenWeb.Data;
using ExamenWeb.Models;

namespace ExamenWeb.Pages.Celular
{
    public class CreateModel : PageModel
    {
        private readonly ExamenWeb.Data.ExamenContext _context;

        public CreateModel(ExamenWeb.Data.ExamenContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Celulares Celulares { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.Celular == null || Celulares == null)
            {
                return Page();
            }

            _context.Celular.Add(Celulares);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
