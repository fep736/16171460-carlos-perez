﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ExamenWeb.Data;
using ExamenWeb.Models;

namespace ExamenWeb.Pages.Celular
{
    public class EditModel : PageModel
    {
        private readonly ExamenWeb.Data.ExamenContext _context;

        public EditModel(ExamenWeb.Data.ExamenContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Celulares Celulares { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Celular == null)
            {
                return NotFound();
            }

            var celular =  await _context.Celular.FirstOrDefaultAsync(m => m.ID == id);
            if (celular == null)
            {
                return NotFound();
            }
            Celulares = celular;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Celulares).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CelularExists(Celulares.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CelularExists(int id)
        {
          return (_context.Celular?.Any(e => e.ID == id)).GetValueOrDefault();
        }
    }
}
