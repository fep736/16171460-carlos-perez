﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ExamenWeb.Data;
using ExamenWeb.Models;

namespace ExamenWeb.Pages.Celular
{
    public class DeleteModel : PageModel
    {
        private readonly ExamenWeb.Data.ExamenContext _context;

        public DeleteModel(ExamenWeb.Data.ExamenContext context)
        {
            _context = context;
        }

        [BindProperty]
      public Celulares Celular { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Celular == null)
            {
                return NotFound();
            }

            var celular = await _context.Celular.FirstOrDefaultAsync(m => m.ID == id);

            if (celular == null)
            {
                return NotFound();
            }
            else 
            {
                Celular = celular;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.Celular == null)
            {
                return NotFound();
            }
            var celular = await _context.Celular.FindAsync(id);

            if (celular != null)
            {
                Celular = celular;
                _context.Celular.Remove(Celular);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
