﻿namespace ExamenWeb.Models
{
    public class Celulares
    {
        public int ID { get; set; }
        public string nombre { get; set; }
        public string marca { get; set; }
        public string color { get; set; }
        public int CPUs { get; set; }
        public string SO { get; set; }
        public decimal precio { get; set; }
    }
}
